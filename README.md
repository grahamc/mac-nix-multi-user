# Install nix-daemon on a Mac

Note this _only_ works if you don't already have a single user nix
install.

Download the install.sh, and run ./install.sh

It will prompt you for a lot of things, but all confirmations. It
doesn't change your system in any way before asking you.

Check out an actual install log: https://grahamc.gitlab.io/mac-nix-multi-user/index.html
